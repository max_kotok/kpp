import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {
    @Test
    public void checkErasingWords() {
        Main main = new Main();

        int length = 4;
        String plainText = "Word close must be deleted. And this one: valve";
        String expectedResult = "Word  must be deleted. And this one: ";

        String actualResult = Main.eraseWords(plainText, length).toString();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void checkErasingNonWords() {
        Main main = new Main();

        int length = 4;
        String plainText = "Th#is is not a word, it should be safe, and t$%*s is too. But email jafar@gmail.com is not safe, and @there@ too";
        String expectedResult = "Th#is is not a word, it should be safe, and t$%*s is too. But email @.com is not safe, and @@ too";

        String actualResult = Main.eraseWords(plainText, length).toString();

        Assert.assertEquals(expectedResult, actualResult);
    }
}