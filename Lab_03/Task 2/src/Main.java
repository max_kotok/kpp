import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static String eraseWords(String plainText, int lengthToDelete) {
        String regex = "\\b[a-zA-Z&&[^aioyueAIUOYE]]\\w{" + String.valueOf(lengthToDelete) + "}\\b";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(plainText);
        return plainText.replaceAll(regex, "");
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        System.out.println("Enter line:");
        String plainText = in.nextLine();
        System.out.println("Enter the length of words, which will be deleted:");
        int lengthToDelete = in.nextInt() - 1;
        System.out.println("");

        plainText = eraseWords(plainText, lengthToDelete);

        System.out.println(plainText);
        in.close();
    }
}
