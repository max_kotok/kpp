import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String args[]) {

        String plainText = "";
        try {
            File myObj = new File("src/lab-3-1.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String temp = myReader.nextLine();
                plainText += temp + " ";
            }
            myReader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        }

        List<String> words = new ArrayList<>();
        try {
            File myObj = new File("src/lab-3-1.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String temp = myReader.next();
                words.add(temp);
            }
            myReader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Something went wrong...");
            e.printStackTrace();
        }

        List<String> sentences = new ArrayList<>();
        String lastSentence = "";
        String temp =  "" + plainText.charAt(0);

        for (int i = 1; i < plainText.length(); i++)
            if (plainText.charAt(i) == ' ' &&
                    (plainText.charAt(i - 1) == '.') || ((plainText.charAt(i - 1) == '!') || (plainText.charAt(i - 1) == '?'))) {
                sentences.add(temp);
                lastSentence = temp;
                temp = "";
            } else
                temp += plainText.charAt(i);

        List<String> pretendents = new ArrayList<>();
        for (var word: words) {
            if (word.indexOf('@') < 0)
                continue;

            if (word.indexOf('@') != word.lastIndexOf('@'))
                continue;

            String text = "";
            if ((word.charAt(word.length() - 1) == '.' || word.charAt(word.length() - 1) == '!')
                    || word.charAt(word.length() - 1) == '?') {
                for (int i = 0; i < word.length() - 1; i++)
                    text += word.charAt(i);
                pretendents.add(text);
            } else
                pretendents.add(word);
        }

        List<String> emails = new ArrayList<>();
        for (var text: pretendents) {
            int pos = text.indexOf('@');

            if (pos == 0 || pos == text.length() - 1)
                continue;

            if (pos > text.length() - 4)
                continue;

            for (int i = pos + 2; i < text.length() - 1; i++)
                if (text.charAt(i) == '.')
                    emails.add(text);
        }

        System.out.println("Pretenders:");
        for (var text: pretendents)
            System.out.println(text);
        System.out.println("\n\nEmails:");
        for (var text: emails)
            System.out.println(text);
        System.out.println("\n\nNew text:");
        for (var sentence: sentences) {
            boolean flag = false;
            for (var email : emails)
                if (sentence.contains(email))
                    flag = true;
            if (flag)
                System.out.println(lastSentence);
            else
                System.out.println(sentence);
        }
    }
}
