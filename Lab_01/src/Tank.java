public class Tank extends Vehicle {
    private Passive passive;
    Tank() { passive = Passive.ABSENT; }
    Tank(String _name, double _caliber, double _armor, double _oilCost, int _crewNum, Passive _passive) {
        name = _name;
        caliber = _caliber;
        armor = _armor;
        oilCost = _oilCost;
        crewNum = _crewNum;
        passive = _passive;
    }

    @Override
    public void printVehicle() {
        System.out.println("Name: " + name);
        System.out.println("Caliber: " + String.valueOf(caliber));
        System.out.println("Armor: " + String.valueOf(armor));
        System.out.println("Oil cost: " + String.valueOf(oilCost));
        System.out.println("Number of crew: " + String.valueOf(crewNum));
        System.out.println("Type of passive defence: " + String.valueOf(passive) + "\n");
    }
}
