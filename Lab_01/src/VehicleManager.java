import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VehicleManager {
    private List<Vehicle> vehicleList;

    VehicleManager(List<Vehicle> _list) { vehicleList = _list; }

    class ArmorComparator implements Comparator<Vehicle> {
        public int compare(Vehicle v1, Vehicle v2) {
            if (v1.getArmor() < v2.getArmor()) return -1;
            if (v1.getArmor() > v2.getArmor()) return 1;
            return 0;
        }
    }

    static class CaliberComparator implements Comparator<Vehicle> {
        private static final Comparator<?> INSTANCE = new CaliberComparator();

        public int compare(Vehicle v1, Vehicle v2) {
            if (v1.getCaliber() < v2.getCaliber()) return -1;
            if (v1.getCaliber() > v2.getCaliber()) return 1;
            return 0;
        }
    }

    public List<Vehicle> selectEconomVehicles() {
        List<Vehicle> selectedVehicles = new ArrayList<Vehicle>();

        for (Vehicle vehicle : vehicleList) {
            if (vehicle.getOilCost() < 20.000001 && vehicle instanceof BTR)
                selectedVehicles.add(vehicle);
        }

        return selectedVehicles;
    }

    // by Anonymous class
    public void sortByOilCost() {
        Collections.sort(vehicleList, new Comparator<Vehicle>() {
            @Override
            public int compare(Vehicle v1, Vehicle v2) {
                if (v1.getOilCost() < v2.getOilCost()) return -1;
                if (v1.getOilCost() > v2.getOilCost()) return 1;
                return 0;
            }
        });
    }

    // by Lambda
    public void sortByCrewNum() {
        vehicleList.sort((Vehicle v1, Vehicle v2) -> Integer.compare(v1.getCrewNum(), v2.getCrewNum()));
    }

    // by Inner class
    public void sortByArmor() {
        vehicleList.sort(new ArmorComparator());
    }

    // by Static inner class
    public void sortByCaliber() {
        vehicleList.sort((Comparator<? super Vehicle>) CaliberComparator.INSTANCE);
    }

    public void sortByCrewNumDesc() {
        vehicleList.sort((Vehicle v1, Vehicle v2) -> Integer.compare(-1 * v1.getCrewNum(), -1 * v2.getCrewNum()));
    }

    public void sortByOilCostDesc() {
        vehicleList.sort((Vehicle v1, Vehicle v2) -> Double.compare(-1 * v1.getOilCost(), -1 * v2.getOilCost()));
    }

    public void sortByArmorDesc() {
        vehicleList.sort((Vehicle v1, Vehicle v2) -> Double.compare(-1 * v1.getArmor(), -1 * v2.getArmor()));
    }

    public void sortByCaliberDesc() {
        vehicleList.sort((Vehicle v1, Vehicle v2) -> Double.compare(-1 * v1.getCaliber(), -1 * v2.getCaliber()));
    }
}
