public class BTR extends Vehicle {
    private int pairOfWheels;
    BTR() { pairOfWheels = 0; }
    BTR(String name, double caliber, double armor, double oilCost, int crewNum, int pairOfWheels) {
        this.name = name;
        this.caliber = caliber;
        this.armor = armor;
        this.oilCost = oilCost;
        this.crewNum = crewNum;
        this.pairOfWheels = pairOfWheels;
    }

    @Override
    public void printVehicle() {
        System.out.println("Name: " + name);
        System.out.println("Caliber: " + String.valueOf(caliber));
        System.out.println("Armor: " + String.valueOf(armor));
        System.out.println("Oil cost: " + String.valueOf(oilCost));
        System.out.println("Number of crew: " + String.valueOf(crewNum));
        System.out.println("Number of wheels: " + String.valueOf(2 * pairOfWheels) + "\n");
    }
}
