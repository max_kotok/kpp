public class Vehicle {
    protected double caliber;
    protected double armor;
    protected double oilCost;
    protected int crewNum;
    protected String name;

    Vehicle() { name = ""; caliber = 0.0; armor = 0.0; oilCost = 0.0; crewNum = 0; }

    Vehicle(String name, double caliber, double armor, double oilCost, int crewNum) {
        this.name = name;
        this.caliber = caliber;
        this.armor = armor;
        this.oilCost = oilCost;
        this.crewNum = crewNum;
    }

    public String getName() { return name; }
    public double getCaliber() { return caliber; }
    public double getArmor() {
        return armor;
    }
    public double getOilCost() {
        return oilCost;
    }
    public int getCrewNum() { return crewNum; }

    public void printVehicle() {
        System.out.println("Name: " + name);
        System.out.println("Caliber: " + String.valueOf(caliber));
        System.out.println("Armor: " + String.valueOf(armor));
        System.out.println("Oil cost: " + String.valueOf(oilCost));
        System.out.println("Number of crew: " + String.valueOf(crewNum) + "\n");
    }
}