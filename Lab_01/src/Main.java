import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        List<Vehicle> vehicles = new ArrayList<>();
        VehicleManager manager = new VehicleManager(vehicles);
        vehicles.add(new BTR("BTR-80",20, 20, 18.4, 4, 4));
        vehicles.add(new BTR("BTR-4",40, 30, 22.8, 3, 4));
        vehicles.add(new BTR("BTR-70",12.7, 20, 17.9, 4, 3));
        vehicles.add(new BMP("BMP-1",75, 40, 18.2, 4, ADS.JAGER));
        vehicles.add(new BMP("BMP-2",13.7, 50, 19.7, 4, ADS.CLASS2));
        vehicles.add(new Tank("T-64BV",115, 500, 26.4, 5, Passive.KNIFE2));
        vehicles.add(new BTR("BTR-60",12.7, 20, 21.6, 3, 2));
        vehicles.add(new Tank("T-80",120, 450, 15, 5, Passive.DUPLET));
        vehicles.add(new Tank("BM Oplot",125, 600, 15, 5, Passive.KNIFE));
        vehicles.add(new BMP("BMP-2A",25, 45, 22.7, 4, ADS.CLASS1));

        boolean _flag = true;

        while (_flag) {
            System.out.println("Here is list of vehicles in this mechanized brigade.");
            System.out.println("Press certain keys to sort it differently.");
            System.out.println("Press 0 to search vehicles with wheels, which spend oil less 20 litres per 100km.");
            System.out.println("Sort by Caliber (static inner class) - 1, descending order - 2");
            System.out.println("Sort by Armor (inner class) - 3, descending order - 4");
            System.out.println("Sort by Spending Oil (anonymous class) - 5, descending order - 6");
            System.out.println("Sort by Number of crew members (using lambda) - 7, descending order - 8");

            Scanner in = new Scanner(System.in);
            int key = in.nextInt();
            boolean flag = true;

            while (flag)
                switch (key) {
                    case 0:
                        flag = false;
                        break;
                    case 1:
                        flag = false;
                        manager.sortByCaliber();
                        break;
                    case 2:
                        flag = false;
                        manager.sortByCaliberDesc();
                        break;
                    case 3:
                        flag = false;
                        manager.sortByArmor();
                        break;
                    case 4:
                        flag = false;
                        manager.sortByArmorDesc();
                        break;
                    case 5:
                        flag = false;
                        manager.sortByOilCost();
                        break;
                    case 6:
                        flag = false;
                        manager.sortByOilCostDesc();
                        break;
                    case 7:
                        flag = false;
                        manager.sortByCrewNum();
                        break;
                    case 8:
                        flag = false;
                        manager.sortByCrewNumDesc();
                        break;
                    default:
                        System.out.println("You typed incorrect value. Try again.\n Press any key...");
                        key = in.nextInt();
                }

            if (key == 0) {
                List<Vehicle> searchedVehicles = manager.selectEconomVehicles();
                for (Vehicle vehicle : searchedVehicles)
                    vehicle.printVehicle();
            } else {
                for (Vehicle vehicle : vehicles)
                    vehicle.printVehicle();
            }

            System.out.println("\nPress any key...\n");
            key = in.nextInt();
            System.out.flush();
        }
    }
}