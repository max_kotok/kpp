public class BMP extends Vehicle {
    private ADS system;
    BMP() { system = ADS.ABSENT; }
    BMP(String _name, double _caliber, double _armor, double _oilCost, int _crewNum, ADS _system) {
        name = _name;
        caliber = _caliber;
        armor = _armor;
        oilCost = _oilCost;
        crewNum = _crewNum;
        system = _system;
    }

    @Override
    public void printVehicle() {
        System.out.println("Name: " + name);
        System.out.println("Caliber: " + String.valueOf(caliber));
        System.out.println("Armor: " + String.valueOf(armor));
        System.out.println("Oil cost: " + String.valueOf(oilCost));
        System.out.println("Number of crew: " + String.valueOf(crewNum));
        System.out.println("Type of active defence: " + String.valueOf(system) + "\n");
    }
}
