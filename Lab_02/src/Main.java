import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static void main(String[] args){

        List<Club> clubs = new ArrayList<>();

        try {
            File myObj = new File("src/lab-2-1.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.next();
                String data2 = myReader.next();
                String data3 = myReader.next();
                clubs.add(new Club(data, data3, Integer.parseInt(data2)));
            }
            myReader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            File myObj = new File("src/lab-2-2.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.next();
                String data2 = myReader.next();
                String data3 = myReader.next();
                clubs.add(new Club(data, data3, Integer.parseInt(data2)));
            }
            myReader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        List<String> cities = new ArrayList<>();
        for (var club: clubs)
            for (var club1: clubs) {
                if (club.getName().equals(club1.getName()) && !club.getCity().equals(club1.getCity()))
                    if (!cities.contains(club.getCity()))
                        cities.add(club.getCity());
                    else if (!cities.contains(club1.getCity()))
                        cities.add(club1.getCity());
            }


        Map<String, List<Club>> cityMap = new HashMap<>();
        for (var club: clubs)
            if (cityMap.containsKey(club.getCity())) {
                List<Club> list = new ArrayList<>();
                list = cityMap.get(club.getCity());
                list.add(club);
                cityMap.put(club.getCity(), list);
            } else {
                List<Club> list = new ArrayList<>();
                list.add(club);
                cityMap.put(club.getCity(), list);
            }

        Scanner in = new Scanner(System.in);
        int citiesNum = 0;
        for (var city : cities)
            citiesNum++;
        System.out.println("There are " + String.valueOf(citiesNum) + " cities, that have same club name.");
        for (var city : cities)
            System.out.println(city);
        while (true) {
            System.out.println("Type in the limit of clubs to print for city: ");
            int n = in.nextInt();

            if (n < 1) {
                System.out.println("Incorrect value. Try again!");
                continue;
            }

            for (Map.Entry<String, List<Club>> entry : cityMap.entrySet()) {
                System.out.println("Clubs in city " + entry.getKey() + ":");
                int k = 0;
                for (var club : entry.getValue())
                    if (k < n) {
                        club.printClub();
                        k++;
                    } else {
                        break;
                    }
                System.out.println();
            }
            break;
        }
    }
}
