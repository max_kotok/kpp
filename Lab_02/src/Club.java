public class Club {
    private String name;
    private String city;
    private int yearOfFoundation;

    Club(String name, String city, int yearOfFoundation) {
        this.name = name;
        this.city = city;
        this.yearOfFoundation = yearOfFoundation;
    }

    public String getName() { return name; }
    public String getCity() { return city; }
    public int getYearOfFoundation() { return yearOfFoundation; }

    public void printClub() {
        System.out.println(name + ' ' + city + ' ' + String.valueOf(yearOfFoundation));
    }
}
